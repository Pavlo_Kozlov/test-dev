$scriptpath = $MyInvocation.MyCommand.Path
$dir = Split-Path $scriptpath
Write-host "My directory is $dir"

### $dir - stores path where the first script were ran from. Other scripts should be in the same directory ###

cd $dir
Write-host "Running script for copying binaries..."
sleep -S 5
.\bincopy.ps1
Write-host "Running script for creating app pools, sites and rewriting hosts files..."
sleep -S 5
.\app_pools_and_sites.ps1
Write-host "Running script for creating SQL login..."
sleep -S 5
.\sqllogin.ps1
Write-host "Running script for restoring database..."
sleep -S 5
invoke-sqlcmd -inputfile "$dir\restore_db_complete.sql" -serverinstance "localhost"
sleep -S 5
cd C:\inetpub\wwwroot
#del Develop
del Start.ps1
dir
cd $dir
sleep -S 5





