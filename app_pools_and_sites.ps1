Import-Module WebAdministration
Import-module servermanager
Add-windowsfeature web-server –includeallsubfeature
Add-WindowsFeature Web-Scripting-Tools
#------------------------------------------------------------------------------------------APP POOLS CREATION START------------------------------------------------------------------------------------#

#--------------------------------------------------------------------------------------------DEFINING APP POOLS VARIABLES------------------------------------------------------------------------------#
$appPoolPath1 = "C:\inetpub\temp\appPools\crs.local" #httpapppool
$appPoolName1 = "crs.local" #httpapppool
$appPoolPath2 = "C:\inetpub\temp\appPools\cso.local" #httpapppool
$appPoolName2 = "cso.local" #httpapppool
$appPoolPath3 = "C:\inetpub\temp\appPools\cs.local" #httpapppool
$appPoolName3 = "cs.local" #httpapppool
$appPoolPath4 = "C:\inetpub\temp\appPools\ps.local" #httpapppool
$appPoolName4 = "ps.local" #httpapppool

$aspBufferLimit1 = "4194304"
#--------------------------------------------------------------------------------------------DEFINING WEB SITES VARIABLES------------------------------------------------------------------------------#
$httpSitePath1 = "C:\inetpub\wwwroot\crs.local" #httpsite1
$httpSiteTitle1 = "crs.local" #httpsite1
$httpSitePath2 = "C:\inetpub\wwwroot\cso.local" #httpsite1
$httpSiteTitle2 = "cso.local" #httpsite1
$httpSitePath3 = "C:\inetpub\wwwroot\cs.local" #httpsite1
$httpSiteTitle3 = "cs.local" #httpsite1
$httpSitePath4 = "C:\inetpub\wwwroot\ps.local" #httpsite1
$httpSiteTitle4 = "ps.local" #httpsite1


# removes app pool and directory if it exists
#Remove-Item $appPoolPath1 -Recurse -ErrorAction SilentlyContinue
#Remove-Item $appPoolPath2 -Recurse -ErrorAction SilentlyContinue
#Remove-Item $appPoolPath3 -Recurse -ErrorAction SilentlyContinue
#Remove-Item $appPoolPath4 -Recurse -ErrorAction SilentlyContinue

# remove http sites if they exist
#Remove-Item $httpSitePath1 -Recurse -ErrorAction SilentlyContinue
#Remove-Item $httpSitePath2 -Recurse -ErrorAction SilentlyContinue
#Remove-Item $httpSitePath3 -Recurse -ErrorAction SilentlyContinue
#Remove-Item $httpSitePath4 -Recurse -ErrorAction SilentlyContinue


#------------------------------------------------------------------------------------------APP POOLS CREATION--------------------------------------------------------------------#
if(!(Test-Path "$appPoolPath1"))
{
New-Item -ItemType directory  -Path $appPoolPath1

}

if(!(Test-Path "$appPoolPath2"))
{
New-Item -ItemType directory  -Path $appPoolPath2

}

if(!(Test-Path "$appPoolPath3"))
{
New-Item -ItemType directory  -Path $appPoolPath3

}

if(!(Test-Path "$appPoolPath4"))
{
New-Item -ItemType directory  -Path $appPoolPath4

}

Write-Host "...Creating AppPool1: $appPoolName1"
$appPool1 = New-WebAppPool -Name $appPoolName1 
Write-Host "...Creating AppPool2: $appPoolName2" 
$appPool2 = New-WebAppPool -Name $appPoolName2  
Write-Host "...Creating AppPool3: $appPoolName3" 
$appPool3 = New-WebAppPool -Name $appPoolName3  
Write-Host "...Creating AppPool4: $appPoolName4" 
$appPool4 = New-WebAppPool -Name $appPoolName4 
#------------------------------------------------------------------------------------------APP POOLS CREATION END-----------------------------------------------------------------------------#

#------------------------------------------------------------------------------------------WEB SITES CREATION---------------------------------------------------------------------------------#
Write-Host ".SITE: $httpSiteTitle1"
Write-Host ".SITE: $httpSiteTitle2"
Write-Host ".SITE: $httpSiteTitle3"
Write-Host ".SITE: $httpSiteTitle4"

$IPHost=((ipconfig | findstr [0-9].\.)[0]).Split()[-1]

New-Item IIS:\Sites\$httpSiteTitle1 -physicalPath $httpSitePath1  -bindings @{protocol="http";bindingInformation=":80:crs.local"}
Set-ItemProperty IIS:\Sites\$httpSiteTitle1 -name applicationPool -value $appPoolName1

New-Item IIS:\Sites\$httpSiteTitle2 -physicalPath $httpSitePath2  -bindings @{protocol="http";bindingInformation=":80:cso.local"}
Set-ItemProperty IIS:\Sites\$httpSiteTitle2 -name applicationPool -value $appPoolName2

New-Item IIS:\Sites\$httpSiteTitle3 -physicalPath $httpSitePath3  -bindings @{protocol="http";bindingInformation=":80:cs.local"}
Set-ItemProperty IIS:\Sites\$httpSiteTitle3 -name applicationPool -value $appPoolName3

New-Item IIS:\Sites\$httpSiteTitle4 -physicalPath $httpSitePath4  -bindings @{protocol="http";bindingInformation=":80:ps.local"}
Set-ItemProperty IIS:\Sites\$httpSiteTitle4 -name applicationPool -value $appPoolName4


# Create the folder if it doesnt exist.
if(!(Test-Path "$httpSitePath1"))
{
 New-Item $httpSitePath1 -itemType directory

}

# Create the folder if it doesnt exist.
if(!(Test-Path "$httpSitePath2"))
{
 New-Item $httpSitePath2 -itemType directory

}

# Create the folder if it doesnt exist.
if(!(Test-Path "$httpSitePath3"))
{
 New-Item $httpSitePath3 -itemType directory

}

# Create the folder if it doesnt exist.
if(!(Test-Path "$httpSitePath4"))
{
 New-Item $httpSitePath4 -itemType directory

}

Set-ItemProperty "IIS:\apppools\crs.local" -Name "enable32BitAppOnWin64" -Value "True"
Set-ItemProperty "IIS:\apppools\cso.local" -Name "enable32BitAppOnWin64" -Value "True"
Set-ItemProperty "IIS:\apppools\cs.local" -Name "enable32BitAppOnWin64" -Value "True"
Set-ItemProperty "IIS:\apppools\ps.local" -Name "enable32BitAppOnWin64" -Value "True"
#------------------------------------------------------------------------------------------WEB SITES CREATION END------------------------------------------------------------------------------#


#---------------------------------------------------------------------------------ENABLE WIN AUTH FOR CSO AND DISABLE ANONYMOUS----------------------------------------------------------------#

Set-WebConfigurationProperty -filter "/system.WebServer/security/authentication/WindowsAuthentication" -name Enabled -value 1 -location "cso.local"
Set-WebConfigurationProperty -filter "/system.WebServer/security/authentication/AnonymousAuthentication" -name Enabled -value 0 -location "cso.local"
	

#-----------------------------------------------------------------------------------------CHECK IP ADDRESS-------------------------------------------------------------------------------------#
Read-Host "Press ENTER to see your ip address"
#Get-WmiObject -Class Win32_NetworkAdapterConfiguration -Filter IPEnabled=TRUE -ComputerName . | Format-Table -Property IPAddress
#$IPHost = Test-Connection -ComputerName (hostname) -Count 1  | Select IPV4Address
#$IPHost=((ipconfig | findstr [0-9].\.)[0]).Split()[-1]

$IPHost
Read-Host "Press ENTER to continue...."

#--------------------------------------------------------------------------------------------MODIFYING HOSTS LOCALLY---------------------------------------------------------------------------#
Write-Host "Modyfying hosts file locally...."
$file = "$env:windir\System32\drivers\etc\hosts"
"$IPHost CRS $httpSiteTitle1" | Add-Content -PassThru $file
"$IPHost CSO $httpSiteTitle2" | Add-Content -PassThru $file
"$IPHost CS  $httpSiteTitle3"  | Add-Content -PassThru $file
"$IPHost PS  $httpSiteTitle4"  | Add-Content -PassThru $file
Read-Host "Press ENTER to continue...."

#--------------------------------------------------------------------------------------------MODIFYING HOSTS REMOTELY--------------------------------------------------------------------------#
Write-Host "Modyfying hosts file remotely...."
$RemoteIp=Read-Host "Enter your Windows 10 IP Address (open cmd on windows 10 machine and type ipconfig)"
$RemoteIp
#robocopy  C:\Windows\System32\drivers\etc\hosts \\EPUALVIW1681\c$\Windows\System32\drivers\etc /e
$Hostname = [System.Net.Dns]::GetHostByAddress("$RemoteIP").Hostname
$Hostname
$RemotePC=$Hostname.Substring(0, $HostName.IndexOf('.')) #removes everything from string till the first dot
$RemotePC
If(Test-Path "\\$RemotePC\c$\Windows\System32\drivers\etc\hosts")
{
del \\$RemotePC\c$\Windows\System32\drivers\etc\hosts
Write-Host "Removed remote hosts file successfully"
}

Copy-Item C:\Windows\System32\drivers\etc\hosts \\$RemotePC\c$\Windows\System32\drivers\etc
Read-Host "Press ENTER to continue...."

invoke-command -scriptblock {iisreset}  #----------------restarting IIS------------------#
