#import SQL Server module
Import-Module SQLPS -DisableNameChecking
 
#your SQL Server Instance Name
$SQLInstanceName = "localhost"
$Server  = New-Object -TypeName Microsoft.SqlServer.Management.Smo.Server -ArgumentList $SQLInstanceName
 
#Specify Database Login Name
$Login_Name = Read-Host "Enter Login Name (create login called Kofile): "
 
#Drop Login if exists
if ($Server.Logins.Contains($Login_Name))
{
$Server.Logins[$Login_Name].Drop()
}
 
#Create an SMO Login object
$Login = New-Object -TypeName Microsoft.SqlServer.Management.Smo.Login -ArgumentList $Server, $Login_Name
 
#identify what type of login this is.
#The possible LoginTypes are AsymmetricKey, Certificate, SQLLogin, WindowsGroup, and WindowsUser
#We are using using a SQLLogin for this turorial
$Login.LoginType = [Microsoft.SqlServer.Management.Smo.LoginType]::SqlLogin
 
#Login object also has a few properties, such as PasswordPolicyEnforced and PasswordExpirationEnabled.
$Login.PasswordExpirationEnabled = $false
$Login.PasswordPolicyEnforced = $false
 
#Prompt the user for the password instead of hardcoding it
#collect Password using a Read-Host cmdlet.
$Password = Read-Host "Write password for Kofile which is provided to you by team member or can be found in configuration docs (for connection to sql instance): " ľAsSecureString
 
$Login.Create($Password)
sleep -S 2
Write-Host "Finished."
sleep -S 10
  